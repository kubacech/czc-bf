'use strict';

const puppeteer = require('puppeteer');

var PRODUCT_URL = 'https://www.czc.cz/xiaomi-mi-electric-scooter-black-elektro-kolobezka-cerna/228231a/produkt'
var LOGIN = ''
var PASS = ''
var PAGE_COUNT = 1

async function run() {
	log('STARTING ...')

	var args = process.argv.slice(2);
	LOGIN = args[0]
	PASS = args[1]
	PAGE_COUNT = args[2]

	const browser = await puppeteer.launch({
		args: ['--no-sandbox'],
	  	headless: true, //set false to see whats happening
	});

	var promises = [];
	for (var i = 0; i < PAGE_COUNT; i++) { 
    	var page = await openProduct(browser)
		await loginIfNeeded(page)
		promises.push(tryBuy(page))
	}

	await Promise.all(promises)

	log("JOB DONE")
	await browser.close()
}

async function tryBuy(page) {
	log('searching for buy button')
	var selector = '.pd-price-delivery #product-price-and-delivery-section .btn-buy'
	var buttonFound = false

	while (!buttonFound) {
		var title = await page.evaluate((a)=> {
			return document.querySelector(a).getAttribute("data-label");
		}, selector);
		if(title === 'Vyčkejte začátku prodeje') {
			log('buy button not found yet ... refreshing ...')
			await page.reload({timeout: 60000, waitUntil: 'domcontentloaded'})
		} else {
			buttonFound = true
		}
	}

	log(' Adding to basket ...')
	const [response1] = await Promise.all([
		  page.waitForNavigation(),
		  page.click(selector)
		]);

	log(' To basket ...')
	await page.goto('https://www.czc.cz/kosik' ,{timeout: 60000, waitUntil: 'domcontentloaded'})

	//validate ...
	log('ITS IN THE BASKET... DO THE REST BY HAND!!!!!!!!!!')
}

async function loginIfNeeded(page) {
	log('checking login')
	var loggedIn = await page.$('.blue-menu #logged-user')
	if (!loggedIn) {
		log('Not logged in. Loggin in right now!!!')
		var loginb = await page.$('.blue-menu #login')
		await loginb.click();
		await page.waitForSelector('.popup-content #login-form-content-container .login-form .submit')

		await page.evaluate((login, pass) => {
	      document.querySelector('.popup-content #login-form-content-container #frm-name').value = login;
	      document.querySelector('.popup-content #login-form-content-container #frm-password-popup').value = pass;
	    }, LOGIN, PASS);
	    const [response] = await Promise.all([
		  page.waitForNavigation(),
		  page.click('.popup-content #login-form-content-container .login-form .submit')
		]);
	}
}

async function openProduct(browser) {
	const page = await browser.newPage()
	await page.setViewport({width: 1800, height:1000})
	await page.goto(PRODUCT_URL ,{timeout: 60000, waitUntil: 'domcontentloaded'})
	return page
}

function log(text) {
    console.log(text)
}

(async() => {
	run()
})();
